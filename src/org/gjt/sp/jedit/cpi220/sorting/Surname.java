package org.gjt.sp.jedit.cpi220.sorting;

public class Surname implements Comparable {

	private String surname;
	private float percent;
	private float cumulative;
	private int rank;
	
	
	
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public float getPercent() {
		return percent;
	}
	public void setPercent(float percent) {
		this.percent = percent;
	}
	public float getCumulative() {
		return cumulative;
	}
	public void setCumulative(float cumulative) {
		this.cumulative = cumulative;
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
	
	@Override
	public int compareTo(Object arg0) {
		Surname s = (Surname) arg0;
		return this.getSurname().compareTo(s.getSurname());
	}
	
	
	
}
